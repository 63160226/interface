/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Car extends Vahicle implements Runable{

    public Car(String Engine) {
        super(Engine);
    }

    @Override
    public void StartEngine() {
        System.out.println("Car :Start");
    }

    @Override
    public void ralsespeed() {
        System.out.println("Car :speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car :Break");
    }

    @Override
    public void StopEngine() {
        System.out.println("Car :Stop");
    }

    @Override
    public void run() {
        System.out.println("Car :Run");
    }
    
}
