/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public abstract class Vahicle {
    private String Engine;
    
    public Vahicle(String Engine){
        this.Engine=Engine;
    }

    public String getEngine() {
        return Engine;
    }

    public void setEngine(String Engine) {
        this.Engine = Engine;
    }
    
    public abstract void StartEngine();
    public abstract void ralsespeed();
    public abstract void applyBreak();
    public abstract void StopEngine();
}
