/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Plane extends Vahicle implements Flyable,Runable{

    public Plane(String Engine) {
        super(Engine);
    }

    @Override
    public void StartEngine() {
        System.out.println("Plane :Start ");
    }

    @Override
    public void ralsespeed() {
        System.out.println("Plane :Speed ");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane :Break ");
    }

    @Override
    public void StopEngine() {
        System.out.println("Plane :Stop ");
    }

    @Override
    public void fly() {
        System.out.println("Plane :Fly");
    }

    public void run() {
        System.out.println("Plane :Run");
    }
    
}
