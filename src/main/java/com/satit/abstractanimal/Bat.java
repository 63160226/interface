/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Bat extends poultryAnimal implements Flyable{

    private String name;

    public Bat(String name) {
        super("Bat", 2);
        this.name = name;
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + name + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bat: " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bat: " + name + " walk");
    }

    @Override
    public void speed() {
        System.out.println("Bat: " + name + " speed");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + name + " sleep");
    }

}
