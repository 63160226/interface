/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.satit.abstractanimal;

/**
 *
 * @author Satit Wapeetao
 */
public class Bird extends poultryAnimal implements Flyable{

    private String name;

    public Bird(String name) {
        super("Bird", 2);
        this.name = name;
    }

    @Override
    public void fly() {
        System.out.println("Bird: " + name + " fly");
    }

    @Override
    public void eat() {
        System.out.println("Bird: " + name + " eat");
    }

    @Override
    public void walk() {
        System.out.println("Bird: " + name + " walk");
    }

    @Override
    public void speed() {
        System.out.println("Bird: " + name + " speed");
    }

    @Override
    public void sleep() {
        System.out.println("Bird: " + name + " sleep");
    }

}
